package smuts;
import java.util.ArrayList;
public class SpreadInMethodWrongName {
  public static void main(String[] args) {
    SpreadInMethodWrongName t = new SpreadInMethodWrongName();
  }
  public SpreadInMethodWrongName() {
    ArrayList<Person> lista = new ArrayList<Person>(10);
    for (int i = 0; i < 10; i++) {
      lista.add(new Person());
    }
    
    ArrayList<Integer> ages = getAges(lista);
  }

  public ArrayList<Integer> getAges(ArrayList<Person> persons) {
    return person*.age;
  }
}

public class Person {
  public int age;

  public Person() {
    this.age = 5;
  }

}


/*EXPECTED
SpreadInMethodWrongName.java:17,12: error: no field named person is accessible
SpreadInMethodWrongName.java:17,20: error: no field named age is accessible
*/
