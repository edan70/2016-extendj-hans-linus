package smuts;
import java.util.ArrayList;
import java.util.List;
public class NonIterableTarget {
  public static void main(String[] args) {
    NonIterableTarget t = new NonIterableTarget();
  }
  public NonIterableTarget() {
    ArrayList<Person> lista = new ArrayList<Person>(10);
    for (int i = 0; i < 10; i++) {
      lista.add(new Person());
    }
    
     int x = lista*.age;
    
  }
}

public class Person {
  public int age;

  public Person() {
    this.age = 7;
  }

}


/*EXPECTED
NonIterableTarget.java:14: error: can not assign variable x of type int a value of type java.util.ArrayList<java.lang.Integer>
NonIterableTarget.java:14: error: Illegal use of spread operator, target type is non-iterable: int
*/
