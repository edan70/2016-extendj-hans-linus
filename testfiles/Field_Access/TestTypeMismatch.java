import java.util.ArrayList;
public class TestTypeMismatch {
    public static void main(String[] args) {
        TestTypeMismatch tt = new TestTypeMismatch();
    }

    public TestTypeMismatch() {
        ArrayList<Person> lista = new ArrayList<Person>(10);
        for (int i = 0; i < 10; i++) {
          lista.add(new Person());
        }
        
        ArrayList<Integer> lista2 = lista*.age;   
    }
}

public class Person {
  public String age;

  public Person() {
    this.age = "5";
  }
}

/*EXPECTED
TestTypeMismatch.java:13: error: can not assign variable lista2 of type java.util.ArrayList<java.lang.Integer> a value of type java.util.ArrayList<java.lang.String>
*/
