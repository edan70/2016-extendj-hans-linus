package smuts;
import java.util.ArrayList;
public class SpreadInMethod {
  public static void main(String[] args) {
    SpreadInMethod t = new SpreadInMethod();
  }
  public SpreadInMethod() {
    ArrayList<Person> lista = new ArrayList<Person>(10);
    for (int i = 0; i < 10; i++) {
      lista.add(new Person());
    }
    
    ArrayList<Integer> ages = getAges(lista);
  }

  public ArrayList<Integer> getAges(ArrayList<Person> persons) {
    return persons*.age;
  }
}

public class Person {
  public int age;

  public Person() {
    this.age = 5;
  }

}


/*EXPECTED
*/
