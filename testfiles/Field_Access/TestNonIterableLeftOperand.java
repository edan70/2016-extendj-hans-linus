import java.util.ArrayList;
public class TestNonIterableLeftOperand {
    public static void main(String[] args) {
        TestNonIterableLeftOperand tt = new TestNonIterableLeftOperand();
    }

    public TestNonIterableLeftOperand() {
        ArrayList<Person> lista = new ArrayList<Person>(10);
        for (int i = 0; i < 10; i++) {
          lista.add(new Person());
        }
        int x = 5;
        ArrayList<Integer> lista2 = x*.age;   
    }
}

public class Person {
  public int age;

  public Person() {
    this.age = 5;
  }
}

/*EXPECTED
TestNonIterableLeftOperand.java:13: error: Illegal use of spread operator on non-iterable type: int
TestNonIterableLeftOperand.java:13,40: error: no field named age is accessible
*/
