package smuts;
import java.util.ArrayList;
public class SpreadAsArgumentWrongType {
  public static void main(String[] args) {
    SpreadAsArgumentWrongType t = new SpreadAsArgumentWrongType();
  }
  public SpreadAsArgumentWrongType() {
    ArrayList<Person> lista = new ArrayList<Person>(10);
    for (int i = 0; i < 10; i++) {
      lista.add(new Person());
    }
    
    printAges(lista*.age);
  }

  public void printAges(ArrayList<Double> ages) {
    for (Double i : ages) {
      System.out.println(i);
    }
  }
}

public class Person {
  public int age;

  public Person() {
    this.age = 5;
  }

}


/*EXPECTED
SpreadAsArgumentWrongType.java:13: error: no method named printAges(java.util.ArrayList<java.lang.Integer>) in smuts.SpreadAsArgumentWrongType matches. However, there is a method printAges(java.util.ArrayList<java.lang.Double>)
*/
