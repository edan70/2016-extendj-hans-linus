package smuts;
import java.util.ArrayList;
public class SpreadAsArgument {
  public static void main(String[] args) {
    SpreadAsArgument t = new SpreadAsArgument();
  }
  public SpreadAsArgument() {
    ArrayList<Person> lista = new ArrayList<Person>(10);
    for (int i = 0; i < 10; i++) {
      lista.add(new Person());
    }
    
    printAges(lista*.age);
  }

  public void printAges(ArrayList<Integer> ages) {
    for (Integer i : ages) {
      System.out.println(i);
    }
  }
}

public class Person {
  public int age;

  public Person() {
    this.age = 5;
  }

}


/*EXPECTED
*/
