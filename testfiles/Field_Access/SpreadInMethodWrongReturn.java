package smuts;
import java.util.ArrayList;
public class SpreadInMethodWrongReturn {
  public static void main(String[] args) {
    SpreadInMethodWrongReturn t = new SpreadInMethodWrongReturn();
  }
  public SpreadInMethodWrongReturn() {
    ArrayList<Person> lista = new ArrayList<Person>(10);
    for (int i = 0; i < 10; i++) {
      lista.add(new Person());
    }
    
    ArrayList<Integer> ages = getAges(lista);
  }

  public ArrayList<Double> getAges(ArrayList<Person> persons) {
    return persons*.age;
  }
}

public class Person {
  public int age;

  public Person() {
    this.age = 5;
  }

}


/*EXPECTED
SpreadInMethodWrongReturn.java:13: error: can not assign variable ages of type java.util.ArrayList<java.lang.Integer> a value of type java.util.ArrayList<java.lang.Double>
SpreadInMethodWrongReturn.java:17: error: return value must be an instance of java.util.ArrayList<java.lang.Double> which java.util.ArrayList<java.lang.Integer> is not
*/
