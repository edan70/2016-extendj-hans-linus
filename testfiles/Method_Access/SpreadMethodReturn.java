import java.util.ArrayList;
public class SpreadMethodReturn {
    public SpreadMethodReturn() {
        ArrayList<Person> persons = new ArrayList<Person>();
        for (int i=0; i<10; i++) {
            persons.add(new Person());
        }
        ArrayList<Integer> list = doSpread(persons);
    }


    public ArrayList<Integer> doSpread(ArrayList<Person> persons) {
        return persons*.getAge();
    }
}

public class Person {
    private int age;
    public Person() {
      age = 5;
    }
    public int getAge() {
      return age;
    }
}

/*EXPECTED
*/
