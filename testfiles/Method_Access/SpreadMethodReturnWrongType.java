import java.util.ArrayList;
public class SpreadMethodReturnWrongType {
    public SpreadMethodReturnWrongType() {
        ArrayList<Person> persons = new ArrayList<Person>();
        for (int i=0; i<10; i++) {
            persons.add(new Person());
        }
        ArrayList<Double> list = persons*.getAge();
    }
}

public class Person {
    private int age;
    public Person() {
      age = 5;
    }
    public int getAge() {
      return age;
    }
}

/*EXPECTED
SpreadMethodReturnWrongType.java:8: error: can not assign variable list of type java.util.ArrayList<java.lang.Double> a value of type java.util.ArrayList<java.lang.Integer>
*/
