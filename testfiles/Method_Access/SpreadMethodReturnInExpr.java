import java.util.ArrayList;
public class SpreadMethodReturnInExpr {
    public SpreadMethodReturnInExpr() {
        ArrayList<Person> persons = new ArrayList<Person>();
        for (int i=0; i<10; i++) {
            persons.add(new Person());
        }

        for (Integer i : persons*.getAge()) {
          System.out.println(i);
        }
    }
}

public class Person {
    private int age;
    public Person() {
      age = 5;
    }
    public int getAge() {
      return age;
    }
}

/*EXPECTED
*/
