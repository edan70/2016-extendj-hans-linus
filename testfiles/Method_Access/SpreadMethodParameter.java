import java.util.ArrayList;
public class SpreadMethodParameter {
    public SpreadMethodParameter() {
        ArrayList<Person> persons = new ArrayList<Person>();
        for (int i=0; i<10; i++) {
            persons.add(new Person());
        }
        persons*.printName("\t\n");
    }
}

public class Person {
    private String name;
    public Person() {
        name="person";
    }
    public void printName(String format) {
        System.out.println(format + name);        
    }
}

/*EXPECTED
*/
