import java.util.ArrayList;
public class SpreadMethodArgument {
    public SpreadMethodArgument() {
        ArrayList<Person> persons = new ArrayList<Person>();
        for (int i=0; i<10; i++) {
            persons.add(new Person());
        }
        printAges(persons*.getAge());
    }


    public void printAges(ArrayList<Integer> ages) {
        for (Integer i : ages) {
            System.out.println(i);
        }
    }
}

public class Person {
    private int age;
    public Person() {
      age = 5;
    }
    public int getAge() {
      return age;
    }
}

/*EXPECTED
*/
