import java.util.ArrayList;
public class MultipleSpreads {
    public MultipleSpreads() {
        ArrayList<Person> persons = new ArrayList<Person>();
        for (int i=0; i<10; i++) {
            persons.add(new Person());
        }

        persons*.getCar()*.printInfo();
        
    }
}

public class Person {
    private Car car;
    public Person() {
      car = new Car(1999, "Ford Mustang");
    }
    public Car getCar() {
      return car;
    }
}

public class Car {
    private int year;
    private String model;

    public Car(int year, String model) {
        this.year = year;
        this.model = model;
    }

    public void printInfo() {
        System.out.println("Model: " + model + ".\n Year: " + year + ".");
    }
}
/*EXPECTED
*/
