import java.util.ArrayList;
public class SpreadMethodReturnInExprWrongType {
    public SpreadMethodReturnInExprWrongType() {
        ArrayList<Person> persons = new ArrayList<Person>();
        for (int i=0; i<10; i++) {
            persons.add(new Person());
        }

        for (Double i : persons*.getAge()) {
          System.out.println(i);
        }
    }
}

public class Person {
    private int age;
    public Person() {
      age = 5;
    }
    public int getAge() {
      return age;
    }
}

/*EXPECTED
SpreadMethodReturnInExprWrongType.java:9: error: parameter of type java.lang.Double can not be assigned an element of type java.lang.Integer
*/
