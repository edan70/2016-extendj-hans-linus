Spread operator extension for ExtendJ
======================

This project implements frontend for the spread operator (*.) in Java using ExtendJ.

Cloning this Project
--------------------

To clone this project you will need [Git][2] installed.

Use this command to clone the project using Git:

    git clone --recursive https://bitbucket.org/edan70/2016-extendj-hans-linus.git

The `--recursive` flag makes Git also clone the ExtendJ submodule while cloning
the `2016-extendj-hans-linus` repository.

If you forgot the `--recursive` flag you can manually clone the ExtendJ
submodule using these commands:

    cd extension-base
    git submodule init
    git submodule update

This should download the ExtendJ Git repository into a local directory named
`extendj`.

Build and Run
-------------

If you have [Gradle][1] installed you can issue the following commands to
build and test the extension:

    gradle jar
    java -jar spread.jar testfiles/Field_Access/SpreadSimple1.java

The last command should print

    testfiles/Field_Access/SpreadSimple1.java contained no errors

If you do not have Gradle installed you can use the `gradlew.bat` (on Windows)
or `gradlew` (Mac/Linux) script instead. For example to build on Windows run the
following in a command prompt:

    gradlew jar

The `gradlew` scripts are wrapper scripts that will download Gradle locally and
run it.

Build and Run the test suite
-------------

If you have [Gradle][1] installed you can issue the following commands to
build and run the automated test suite:

    gradle test

This runs all test files in the testfiles directory and creates a report on the test results located in
    
    build/reports/tests/index.html

If you do not have Gradle installed you can use the following instead

    gradlew test

File Overview
-------------

Here is a short explanation of the purpose of each file in the project:

* `build.gradle` - the main Gradle build script. There is more info about this below.
*  `jastadd_modules` - this file contains module definitions for the JastAdd build tool. This
  defines things such as which ExtendJ modules to include in the build, and where
additional JastAdd source files are located.
* `README.md` - this file.
* `gradlew.bat` - Windows Gradle wrapper script (explained above)
* `gradlew` - Unix Gradle wrapper script
* `src/java/org/extendj/ExtensionMain.java` - main class for the base extension. Parses
  Java files supplied on the command-line and runs the `process()` method on each parsed AST.
* `src/jastadd/` - the aspects and abstract grammar definitions
* `src/parser/` - the grammar for the spread operator
* `src/scanner/` - the token definition of the spread operator
* `testfiles/` - two categories of tests using the spread operator


[1]:https://gradle.org/
[2]:https://git-scm.com/
