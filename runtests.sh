#!/bin/bash
declare -a errors
RED='\033[0;31m'
LRED='\033[1;31m'
GREEN='\033[1;32m'
CYAN='\033[1;36m'
YELLOW='\033[1;33m'
NC='\033[0m'
DIR="./testfiles/"
if [ "$1" = "clean" ]
then
echo "Cleaning houueaash"
    for f in $DIR*.{out,class}
    do
        rm -f $f
    done
    exit 0
else
    for f in $DIR*.java
    do
        echo -e "${CYAN}Compiling $f...${NC}"
        java -jar spread.jar $f &> ${f%.java}".out"
    done
    echo -e "${CYAN}Done!${NC}"
    echo -e "\nTest results:\n"
    for f in $DIR*.java
    do
        DIFF=$(diff ${f%.*}".out" ${f%.*}".expected")
        if [ "$DIFF" = "" ]
        then
            echo -e "${GREEN}passed: ${NC}"${f%.*}
        else
            res='echo -e "${LRED}failed: ${NC}"'
            res+=${f%.*}
            res+=';'
            res+='echo -e "${YELLOW}Expected:${NC}";'
            res+='cat '
            res+=${f%.*}
            res+='".expected";'
            res+='printf "\n";'
            res+='echo -e "${YELLOW}Encountered:${NC}";'
            res+='cat '
            res+=${f%.*}
            res+='".out";'
            res+='printf "\n";'
            errors+=$res
        fi
    done
    for e in "${errors[@]}"
    do
        eval $e
    done
   
fi
