/* Copyright (c) 2016, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj;

import beaver.Parser;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.FileClassSource;
import org.extendj.ast.Problem;
import org.extendj.ast.Program;
import org.extendj.ast.SourceFilePath;
import org.extendj.scanner.JavaScanner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.extendj.parser.JavaParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;

/**
 * This parameterized JUnit test finds all the .java files in testfiles/type and
 * parses each as a compilation unit. For each compilation unit we check if the
 * compile problems are identical to the problems listed in the EXPECTED comment
 * at the end of the input source file.
 *
 * @author Jesper Öqvist
 */
@RunWith(Parameterized.class)
public class SpreadTypeTest {
  private final File file;
  private final String filename;

  public SpreadTypeTest(File file, String filename) {
    this.file = file;
    this.filename = filename;
  }

  /** Build the list of test parameters (test input files). */
  @Parameterized.Parameters(name="{1}")
  public static Iterable<Object[]> getTests() {
    Collection<Object[]> tests = new LinkedList<>();
    addTests(tests, "testfiles/Field_Access");
    addTests(tests, "testfiles/Method_Access");
    return tests;
  }

  private static void addTests(Collection<Object[]> tests, String dirPath) {
    File testDir = new File(dirPath);
    if (!testDir.isDirectory()) {
      throw new Error("Could not find the test directory '" + testDir + "'");
    }
    File[] files = testDir.listFiles();
    if (files != null) {
      for (File file : files) {
        if (file.getName().endsWith(".java")) {
          tests.add(new Object[] {file, file.getName()});
        }
      }
    }
  }

  @Test
  public void runTest() throws IOException, Parser.Exception {
    List<String> expectedLines = getExpectedLines();
    try (FileReader reader = new FileReader(file)) {
      JavaScanner scanner = new JavaScanner(reader);
      JavaParser parser = new JavaParser();
      Program program = new Program();
      CompilationUnit compilationUnit = (CompilationUnit) parser.parse(scanner);

      // Set a class source so that error messages use the right filename:
      compilationUnit.setClassSource(new FileClassSource(new SourceFilePath(filename), filename));

      // The compilation unit must be attached to a program node:
      program.addCompilationUnit(compilationUnit);

      List<String> problems = compilationUnit.problems().stream().map(Problem::toString)
          .collect(Collectors.toList());
      assertThat(problems).containsExactlyElementsIn(expectedLines);
    }
  }

  public List<String> getExpectedLines() throws IOException {
    List<String> expected = new LinkedList<>();
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      boolean addLines = false;
      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        } else if (addLines) {
          if (line.startsWith("*/")) {
            break;
          }
          expected.add(line);
        } else if (line.startsWith("/*EXPECTED")) {
          addLines = true;
        }
      }
    }
    return expected;
  }
}
