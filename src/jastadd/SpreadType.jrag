aspect Type {
  
  syn GenericTypeDecl TypeDecl.asGenericType() = null;
  eq GenericTypeDecl.asGenericType() = this;
  
  /**
   * Computes the complete type of the collection returned.
   */
  syn TypeDecl SpreadExpr.type() {
    TypeDecl collection = lookupType(collectionPackageName(), collectionTypeName());
    GenericTypeDecl generic = collection.asGenericType();
    if (generic != null) {
      TypeDecl elementType = getAccess().type();
      if (elementType.boxed().isUnknown()) {
        return generic.lookupParTypeDecl(Collections.singletonList(elementType));
      } else {
        return generic.lookupParTypeDecl(Collections.singletonList(elementType.boxed()));
      }
      } else {
        return unknownType();
    }
  }
  
  /**
   * Complete name of collection returned, such as "ArrayList<Integer>".
   */
  syn String SpreadExpr.typeName() = type().typeName();

  /**
   * The name of the collection type, such as "ArrayList".
   */
  syn String SpreadExpr.collectionTypeName() {
    return getCollection().getTypeName();
  }
  
  syn String VarAccess.getTypeName() {
    return decl().type().original().name();
  }
     
  syn String Expr.getTypeName() = "";

  syn String SpreadExpr.getTypeName() {
    return getCollection().getTypeName();
  }

  
 
  /**
   * The package of the collection type, such as "java.util".
   */
  syn String SpreadExpr.collectionPackageName() {
    return getCollection().getPackageName();
  }
 
  syn String VarAccess.getPackageName() {
    return decl().type().original().packageName();
  }
     
  syn String Expr.getPackageName() = "";

  syn String SpreadExpr.getPackageName() {
    return getCollection().getPackageName();
  }
  
  /**
   * The declaration for the type contained in getCollection().
   */
  syn TypeDecl SpreadExpr.collectionOf() {
    return getCollection().type().iterableElementType();
  }
  
  /**
   * Finds the field accessed through spread operator.
   */
  eq SpreadExpr.getAccess().lookupVariable(String name) {
    return collectionOf().memberFields(name);
  }

  /**
   * Finds the method accessed through spread operator.
   */
  eq SpreadExpr.getAccess().lookupMethod(String name) {
    return collectionOf().memberMethods(name);
  }

}
