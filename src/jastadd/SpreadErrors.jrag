aspect Errors {
    /**
     * Checks whether variable assigned to is iterable
     */
    syn boolean SpreadExpr.targetIsIterable() {
    return targetType().isIterable();
    }

    /**
     * Checks whether Collection operated on is iterable
     */
    syn boolean SpreadExpr.collectionIsIterable() {
    return getCollection().type().isIterable();
    } 
    
    SpreadExpr contributes error("Illegal use of spread operator on non-iterable type: " + getCollection().type().typeName())
    when !collectionIsIterable()
    to CompilationUnit.problems();

    SpreadExpr contributes error("Illegal use of spread operator, target type is non-iterable: " + targetType().typeName())
    when !targetIsIterable()
    to CompilationUnit.problems();

}
